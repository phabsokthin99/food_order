

import { initializeApp } from 'firebase/app'
import { getFirestore, serverTimestamp } from 'firebase/firestore'
import { GoogleAuthProvider, getAuth } from 'firebase/auth'

// project is firebaseCrud in sokthinfootball123@gmail.com
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCYj6zNtCkI7z4H87R5o_t5-ij-Ndwi9kU",
    authDomain: "fir-crud-432b0.firebaseapp.com",
    databaseURL: "https://fir-crud-432b0-default-rtdb.firebaseio.com",
    projectId: "fir-crud-432b0",
    storageBucket: "fir-crud-432b0.appspot.com",
    messagingSenderId: "138897035189",
    appId: "1:138897035189:web:d97c032574c5ace07593fc",
    measurementId: "G-CF1BELHBFE"
};

const app = initializeApp(firebaseConfig)
const projectFireStore = getFirestore(app)
const projectAuth = getAuth(app)
const timestamp = serverTimestamp();
const provider = new GoogleAuthProvider();

export {projectFireStore, projectAuth, timestamp, provider}

