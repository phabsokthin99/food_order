import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './css/index.css';
import VueAwesomePaginate from 'vue-awesome-paginate';
import 'vue-awesome-paginate/dist/style.css';
import { setPersistence, browserLocalPersistence } from 'firebase/auth';
import { projectAuth } from './config/config';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { MotionPlugin } from '@vueuse/motion'
import i18n from './i18n'


// Initialize AOS in the app instance
const initializeAOS = () => {
  AOS.init();
};

setPersistence(projectAuth, browserLocalPersistence)
  .then(() => {
    const app = createApp(App).use(i18n);

    app.use(router);
    app.use(VueAwesomePaginate);
    app.mount('#app');
    app.use(MotionPlugin)

    // Call initializeAOS after mounting the app
    initializeAOS();
  })
  .catch((error) => {
    // Handle any errors related to persistence setting
    console.error('Error setting up session persistence:', error);
  });
