
import {projectFireStore} from '@/config/config'
import {doc, onSnapshot } from "firebase/firestore";
import getUser from "./getUser";
import {ref, watchEffect} from 'vue'


const getCollection = (collection) => {
   const error = ref(null);
   const isPending = ref(true);
   const {user} = getUser()
   const _user = ref(null);
   if (user.value) {

    const documentRef = doc(
      projectFireStore,
      `${collection}/${user.value?.uid}` 
    );
    console.log(user.value.uid)
     
    const unsubscribe = onSnapshot(documentRef, (doc) => {
      if(doc.data()){
        const userData = doc.data(); 
        _user.value = { ...userData }; 
        error.value = null;
        // console.log(_user)
      }
      else{
        error.value = `User with the given UID ${user.value.uid} doesn't exist`;
        _user.value = null;
      }
    }, (err) => {
      // Error handling
      error.value = err.message;
    });
  
    watchEffect((onInvalidate) => {
      onInvalidate(() => unsubscribe());
    });
  }
   return {error, isPending, _user}
}

export default getCollection;

