import { projectFireStore } from "@/config/config"
import { addDoc, collection, deleteDoc, doc, setDoc } from "firebase/firestore"
import { ref } from "vue"


const useCollection = (collectionName) => {

    const error = ref(null)
    const isLoading = ref(false)
    const collectionRef = collection(projectFireStore, collectionName)
    const addDocs = async(formDoc) => {
        try{
            isLoading.value = true
            await addDoc(collectionRef, formDoc)
            isLoading.value = false
            return true;
        }
        catch(err){
            console.log(err)
        }
    }

    const setDocs = async(formDoc,id) => {
        const documentRef = doc(collectionRef, id)
        try{
            isLoading.value = true
            await setDoc(documentRef, formDoc)
            isLoading.value  = false
            return true
        }
        catch(err){
            console.log(err)
            error.value = err.message
        }
    }

    const removeDocs = async(docID) => {

        try{
            await deleteDoc(doc(collectionRef,docID))
            return true;
        }
        catch(err){
            console.log(err)
        }
    }

    return {addDocs, setDocs, error, removeDocs}

}

export default useCollection;