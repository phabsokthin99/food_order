// import { projectFireStore } from "@/config/config"
import { deleteObject,getStorage, ref as imageRef } from "firebase/storage"


const useStorage = () => {

    const storage = getStorage()
    const deleteImage = async (imageURL) => {

        const storageRef = imageRef(storage, imageURL);
        try {
            await deleteObject(storageRef);

        } catch (err) {
            console.log(err)
        }
    }

    return { deleteImage };

}


export default useStorage;