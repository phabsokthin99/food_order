import { ref } from "vue";
import { projectAuth } from "@/config/config";
import { signInWithEmailAndPassword } from "firebase/auth";

const error = ref(null);
const isLoading = ref(false);

const signin = async (email, password) => {
    error.value = null;
    isLoading.value = true;
    await signInWithEmailAndPassword(projectAuth, email, password)
        .then(() => {
            isLoading.value = false;
        })
        .catch((err) => {
            switch (err.code) {
                case 'auth/invalid-email':
                    error.value = "Invalid Email"
                    isLoading.value = false;
                    break;

                case 'auth/user-not-found':
                    error.value = "No account with that email was found"
                    isLoading.value = false;
                    break;

                case 'auth/wrong-password':
                    error.value = "Incorrect Password"
                    isLoading.value = false;
                    break;


                case 'auth/too-many-requests':
                    error.value = "Disabled account auth too many request"
                    isLoading.value = false
                    break

                case 'auth/invalid-credential':
                    error.value = "Account auth/invalid-credential"
                    isLoading.value = false
                    break

                default:
                    error.value = "email or password was incorrect" + err
                    isLoading.value = false;
                    break;
            }
            //   error.value = err.message
            //   isLoading.value = false
        });

    // console.log("Error login",err);
};
//  error.value = "Invalid email or password" + err;
//  isLoading.value = false;

const useSignin = () => {
    return { error, isLoading, signin };
};

export default useSignin;
