import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CategoryDetails from '../views/CategoryDetailView.vue'
import AddCategory from '../components/AddCategoryComponent.vue'
import SignupView from '../views/SignupView.vue'
import LoginView from '@/views/LoginView.vue'
import CategoryView from '@/views/CategoryView.vue'
import PaginationComponent from '@/components/PaginationComponent.vue'
import CartDetailView from '@/views/CartDetailView.vue'
import ForgotPasswordView from '@/views/ForgotPasswordView.vue'
import ChangePassword from '@/views/ChangePasswordView.vue'
import OrderListStatus from '@/views/OrderListStatusView.vue'
import CompoteOrderView from '@/views/CompleteOrderView.vue'
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: "/category_detail/:id/:textSearch?",
    name: "category_detail",
    component: CategoryDetails
  },
  {
    path: "/add_category",
    name: "add_category",
    component: AddCategory
  },
  {
    path: "/signup",
    name: "signup",
    component: SignupView
  },
  {
    path: "/login",
    name: "login",
    component: LoginView
  },
  {
    path: "/category",
    name: "category",
    component: CategoryView
  },
  {
    path: "/paginate",
    name: "paginate",
    component: PaginationComponent
  },
  {
    path: "/cartDetail",
    name: "cartDetail",
    component: CartDetailView
  },
  {
    path: "/forgotPassword",
    name: "forgotPassword",
    component: ForgotPasswordView
  },
  {
    path: "/changepassword",
    name: "changepassword",
    component: ChangePassword
  },
  //admin
  {
    path: "/OrderListStatusView",
    name: 'OrderListStatusView',
    component: OrderListStatus
  },
  {
    path: "/CompoteOrderView",
    name: 'CompoteOrderView',
    component: CompoteOrderView
  }

]


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
