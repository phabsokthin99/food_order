/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'italian-food': "url('https://images.pexels.com/photos/4577394/pexels-photo-4577394.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load')",
      },
    },
  },
  plugins: [],
}
